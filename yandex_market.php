<?php
/**
 * Класс YML экспорта
 * YML (Yandex Market Language) - стандарт, разработанный "Яндексом"
 * для принятия и публикации информации в базе данных Яндекс.Маркет
 * YML основан на стандарте XML (Extensible Markup Language)
 * описание формата YML http://partner.market.yandex.ru/legal/tt/
 */
/*error_reporting(E_ALL);
	error_reporting(E_ALL);
	ini_set("display_errors", 1); */
class ControllerFeedYandexMarket extends Controller {
    private $shop = array();
    private $currencies = array();
    private $categories = array();
    private $offers = array();
    private $from_charset = 'utf-8';
    private $eol = "\n";

    public function index() {

        if ($this->config->get('yandex_market_status')) {

            if (!($allowed_categories = $this->config->get('yandex_market_categories'))) exit();
            //var_dump($allowed_categories);
            //$aIn = explode(",",$allowed_categories);
            //var_dump($aIn);
            //die("");
            $this->response->addHeader('Content-Type: application/xml');

            $utm = $delivery = $deliverycost = '';
            $campaign = 'yandex_market';
            if (isset($_GET['utm'])) {
                $utm = $_GET['utm'];
                $delivery = '0-2';
                $deliverycost = '250';
                if (strstr($utm,'yandex_market')) {
                    $campaign = $utm;
                    $deliverycost = '300';
                    if ($campaign == 'yandex_market_msk') $delivery = '1-3';
                    elseif ($campaign == 'yandex_market_spb') $delivery = '2-4';
                } else $utm = 'utm';
            }
            $filename = DIR_DOWNLOAD . 'offers'.$utm.'.yml';
            $temp_filename = $filename.'.new';
            $iSize = 0;
            if(is_file($filename)){
                $iSize=filesize($filename);
            }
            if ((!is_file($filename) || time() - @filemtime($filename) > 60*30)||$iSize<10) { // one time per half of hour
                $fd = fopen($temp_filename, 'w');

                $this->load->model('export/yandex_market');
                $this->load->model('localisation/currency');
                $this->load->model('tool/image');
                $this->load->model('catalog/product');

                // Магазин
                $this->setShop('name', $this->config->get('yandex_market_shopname'));
                $this->setShop('company', $this->config->get('yandex_market_company'));
                $this->setShop('url', HTTPS_SERVER);
                $this->setShop('phone', $this->config->get('config_telephone'));
                $this->setShop('platform', 'ocStore');
                $this->setShop('version', VERSION);

                // Валюты
                // TODO: Добавить возможность настраивать проценты в админке.
                $offers_currency = $this->config->get('yandex_market_currency');
                $offers_currency = "RUB";

                if (!$this->currency->has($offers_currency)) exit();

                $decimal_place = $this->currency->getDecimalPlace($offers_currency);

                if (!$decimal_place) {
                    $decimal_place = 2;
                }

                $shop_currency = $this->config->get('config_currency');

                $this->setCurrency($offers_currency, 1);

                $currencies = $this->model_localisation_currency->getCurrencies();

                $supported_currencies = array('RUR', 'RUB', 'USD', 'BYR', 'KZT', 'EUR', 'UAH');

                $currencies = array_intersect_key($currencies, array_flip($supported_currencies));

                foreach ($currencies as $currency) {
                    if ($currency['code'] != $offers_currency && $currency['status'] == 1) {
                        $this->setCurrency($currency['code'], number_format(1/$this->currency->convert($currency['value'], $offers_currency, $shop_currency), 4, '.', ''));
                    }
                }
//die($offers_currency);

                // Категории
                $categories = $this->model_export_yandex_market->getCategory($allowed_categories);
                $aAllow = array();
                foreach ($categories as $category) {
                    //print($category['category_id']."<BR>");
                    $aAllow[$category['category_id']]=true;
                    /*switch($category['name']){
                        case "Распродажа":
                        case "Уцененные товары":
                            break;
                        default:

                            break;
                        }*/
                    $this->setCategory($this->fixCapsWords($category['name']), $category['category_id'], $category['parent_id']);
                }

                // Товарные предложения
                $in_stock_id = $this->config->get('yandex_market_in_stock'); // id статуса товара "В наличии"
                $out_of_stock_id = $this->config->get('yandex_market_out_of_stock'); // id статуса товара "Нет на складе"
                $vendor_required = false; // true - только товары у которых задан производитель, необходимо для 'vendor.model'
                $products = $this->model_export_yandex_market->getProduct($allowed_categories, $out_of_stock_id, $vendor_required);
                //print(__LINE__."<BR>");

                $yml  = '<?xml version="1.0" encoding="utf-8"?>' . $this->eol;
                $yml .= '<!DOCTYPE yml_catalog SYSTEM "shops.dtd">' . $this->eol;
                $yml .= '<yml_catalog date="' . date('Y-m-d H:i') . '">' . $this->eol;
                $yml .= '<shop>' . $this->eol;

                // информация о магазине
                $yml .= $this->array2Tag($this->shop);

                // валюты
                $yml .= '<currencies>' . $this->eol;
                foreach ($this->currencies as $currency) {
                    $yml .= $this->getElement($currency, 'currency');
                }
                $yml .= '</currencies>' . $this->eol;

                // категории
                $yml .= '<categories>' . $this->eol;
                foreach ($this->categories as $category) {
                    $category_name = $this->fixCapsWords($category['name']);
                    unset($category['name'], $category['export']);
                    $yml .= $this->getElement($category, 'category', $category_name);
                }
                $yml .= '</categories>' . $this->eol;

                // Доставка в прайсе
                if(!empty($utm)) {
                    $cost = $deliverycost;
                    $days = $delivery;
                    $yml .= '<delivery-options><option cost="'.$cost.'" days="'.$days.'" order-before="9"/></delivery-options>' . $this->eol;
                }

                $yml .= '<offers>' . $this->eol;

                fwrite($fd, $yml);

                $buffer = '';
                $buffer_size = 128*128*1024; // 128Kb
                $ords = [];
                if (!empty($utm)) {
                    $query = $this->db->query("SELECT pf.product_id from " . DB_PREFIX . "filter_description fd LEFT JOIN " . DB_PREFIX . "product_filter pf ON (pf.filter_id = fd.filter_id) LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fgd.filter_group_id = fd.filter_group_id) WHERE fd.name = 'на заказ' AND fgd.name = 'наличие'");
                    foreach($query->rows as $row)
                        $ords[$row['product_id']]=1;
                }
                foreach ($products as $product) {

                    $data = array();

                    // Атрибуты товарного предложения
                    $data['available'] = ($product['quantity'] > 0 || $product['stock_status_id'] == $in_stock_id);
                    if($data['available'] === false)
                        continue;
                    $ord = isset($ords[$product['product_id']]);
//                    if(!empty($utm)) && $ord)
//                        continue;
                    $data['available'] = !$ord;
                    if(!isset($aAllow[$product['category_id']]))
                        continue;
                    $data['id'] = $product['product_id'];
                    //				$data['type'] = 'vendor.model';
                    //				$data['bid'] = 10;
                    //				$data['cbid'] = 15;

                    // Параметры товарного предложения
                    $data['url'] = $this->url->link('product/product', 'path=' . $this->getPath($product['category_id']) . '&product_id=' . $product['product_id']);

                    if(!empty($utm))
                        $data['url'].= (strstr($data['url'],'?') ? '&' : '?') . 'utm_source=market.yandex.ru&utm_medium=cpc&utm_campaign='.$campaign.'&utm_term='.$data['id'];
                    // USE THIS TO SPEED UP: $data['url'] = HTTP_SERVER . 'index.php?route=product/product&path=' . $this->getPath($product['category_id']) . '&product_id=' . $product['product_id'];
                    $iPice = $this->currency->convert($this->tax->calculate($product['price'], $product['tax_class_id']), $shop_currency, $offers_currency);
                    if(((int)$iPice)<=0 || ((int)$iPice)>=1000000000)
                        continue;
                    $data['price'] = number_format($iPice, $decimal_place, '.', '');
                    $data['currencyId'] = $offers_currency;
                    $data['categoryId'] = $product['category_id'];
                    $data['delivery'] = 'true';
                    //				$data['local_delivery_cost'] = 100;
                    $data['name'] = $this->fixCapsWords($product['name']);
                    $data['vendor'] = ($product['manufacturer']=="")?"No name":$this->fixCapsWords($product['manufacturer']);
                    $data['vendorCode'] = $product['model'];
                    $data['model'] = $data['name'];
                    // Если нет описания - кладём в него имя
                    if (isset($product['description']) && strlen($product['description']) > 3) {
                        $data['description'] = str_replace("\n", " ", $this->fixCapsWords($product['description']));
                        $data['description'] = str_replace("<br />", " ", $data['description']);
                    } else {
                        $data['description'] = $data['name'];
                    }
                    $data['manufacturer_warranty'] = 'true';
                    //				$data['barcode'] = $product['sku'];
                    $data['picture'] = "";
                    if ($product['image']) {
                        //$data['picture'] = $this->model_tool_image->resize($product['image'], 500, 500);

                        $image_path = 'image/' . $product['image'];

                        if (strpos($image_path, ' ') !== false) {
                            $new_image_path = str_replace(' ', '__', $image_path);

                            if (!file_exists($new_image_path)) {
                                $new_image_dir = dirname($new_image_path);

                                if (!is_dir($new_image_dir)) {
                                    mkdir($new_image_dir, 0777, true);
                                }

                                copy($image_path, $new_image_path);
                            }

                            $image_path = $new_image_path;
                        }
                        $data['picture'] = HTTPS_SERVER . $image_path;
                    }

                    if($data['picture'] == "")
                        continue;

                    // Если с картинкой "Скоро тут будет фото" - не выгружаем
                    if (filesize('./' . $image_path) == 9114) {
                        continue;
                    }

                    // Если на сервере нет картинки - не выгружаем
                    if (!file_exists('./' . $image_path)) {
                        continue;
                    }

                    $attribute_groups = $this->model_catalog_product->getProductAttributes($product['product_id']);

                    if (!empty($attribute_groups)) {
                        $data['param'] = array();
                        foreach ($attribute_groups as $attribute_group) {
                            foreach ($attribute_group['attribute'] as $attribute) {
                                $data['param'][] = array (
                                    'name'  => $this->fixCapsWords($attribute['name']),
                                    'value' => $this->fixCapsWords($attribute['text']),
                                );
                            }
                        }
                    }
                    /*
                                    // пример структуры массива для вывода параметров
                                    $data['param'] = array(
                                        array(
                                            'name'=>'Wi-Fi',
                                            'value'=>'есть'
                                        ), array(
                                            'name'=>'Размер экрана',
                                            'unit'=>'дюйм',
                                            'value'=>'20'
                                        ), array(
                                            'name'=>'Вес',
                                            'unit'=>'кг',
                                            'value'=>'4.6'
                                        )
                                    );
                    */

                    $offer = $this->setOffer($data);
                    if(!empty($utm)) {
                        $cost = $deliverycost;
                        if(((int)$iPice)>=5000 && in_array($campaign,array('yandex_market_msk','yandex_market_spb')))
                            $cost = '0';
                        $days = $delivery;
                        if ($ord) $days = '';
                        $offer['data']['delivery-options']='<option cost="'.$cost.'" days="'.$days.'"/>';
                    }
                    $tags = $this->array2Tag($offer['data']);
                    unset($offer['data']);
                    if (isset($offer['param'])) {
                        $tags .= $this->array2Param($offer['param']);
                        unset($offer['param']);
                    }

                    $buffer .= $this->getElement($offer, 'offer', $tags);
                    if (strlen($buffer) >= $buffer_size) {
                        fwrite($fd, $buffer);
                        $buffer = '';
                    }
                }

                if ($buffer != '') {
                    fwrite($fd, $buffer);
                }

                $yml = '';
                $yml .= '</offers>' . $this->eol;

                $yml .= '</shop>';
                $yml .= '</yml_catalog>';

                fwrite($fd, $yml);
                unset($yml);
                fclose($fd);

                if(is_file($temp_filename) && filesize($temp_filename)>10) {
                    rename($filename, $filename.'.old');
                    rename($temp_filename, $filename);
                }
            }

            readfile($filename);
            //
            //$this->categories = array_filter($this->categories, array($this, "filterCategory"));
        }
    }

    /**
     * Методы формирования YML
     */

    /**
     * Формирование массива для элемента shop описывающего магазин
     *
     * @param string $name - Название элемента
     * @param string $value - Значение элемента
     */
    private function setShop($name, $value) {
        $allowed = array('name', 'company', 'url', 'phone', 'platform', 'version', 'agency', 'email');
        if (in_array($name, $allowed)) {
            $this->shop[$name] = $this->prepareField($value);
        }
    }

    /**
     * Валюты
     *
     * @param string $id - код валюты (RUR, RUB, USD, BYR, KZT, EUR, UAH)
     * @param float|string $rate - курс этой валюты к валюте, взятой за единицу.
     *	Параметр rate может иметь так же следующие значения:
     *		CBRF - курс по Центральному банку РФ.
     *		NBU - курс по Национальному банку Украины.
     *		NBK - курс по Национальному банку Казахстана.
     *		СВ - курс по банку той страны, к которой относится интернет-магазин
     * 		по Своему региону, указанному в Партнерском интерфейсе Яндекс.Маркета.
     * @param float $plus - используется только в случае rate = CBRF, NBU, NBK или СВ
     *		и означает на сколько увеличить курс в процентах от курса выбранного банка
     * @return bool
     */
    private function setCurrency($id, $rate = 'CBRF', $plus = 0) {
        $allow_id = array('RUR', 'RUB', 'USD', 'BYR', 'KZT', 'EUR', 'UAH');
        if (!in_array($id, $allow_id)) {
            return false;
        }
        $allow_rate = array('CBRF', 'NBU', 'NBK', 'CB');
        if (in_array($rate, $allow_rate)) {
            $plus = str_replace(',', '.', $plus);
            if (is_numeric($plus) && $plus > 0) {
                $this->currencies[] = array(
                    'id'=>$this->prepareField(strtoupper($id)),
                    'rate'=>$rate,
                    'plus'=>(float)$plus
                );
            } else {
                $this->currencies[] = array(
                    'id'=>$this->prepareField(strtoupper($id)),
                    'rate'=>$rate
                );
            }
        } else {
            $rate = str_replace(',', '.', $rate);
            if (!(is_numeric($rate) && $rate > 0)) {
                return false;
            }
            $this->currencies[] = array(
                'id'=>$this->prepareField(strtoupper($id)),
                'rate'=>(float)$rate
            );
        }

        return true;
    }

    /**
     * Категории товаров
     *
     * @param string $name - название рубрики
     * @param int $id - id рубрики
     * @param int $parent_id - id родительской рубрики
     * @return bool
     */
    private function setCategory($name, $id, $parent_id = 0) {
        $id = (int)$id;
        if ($id < 1 || trim($name) == '') {
            return false;
        }
        if ((int)$parent_id > 0) {
            $this->categories[$id] = array(
                'id'=>$id,
                'parentId'=>(int)$parent_id,
                'name'=>$this->prepareField($name)
            );
        } else {
            $this->categories[$id] = array(
                'id'=>$id,
                'name'=>$this->prepareField($name)
            );
        }

        return true;
    }

    /**
     * Товарные предложения
     *
     * @param array $data - массив параметров товарного предложения
     */
    private function setOffer($data) {
        $offer = array();

        $attributes = array('id', 'type', 'available', 'bid', 'cbid', 'param');
        $attributes = array_intersect_key($data, array_flip($attributes));

        foreach ($attributes as $key => $value) {
            switch ($key)
            {
                case 'id':
                case 'bid':
                case 'cbid':
                    $value = (int)$value;
                    if ($value > 0) {
                        $offer[$key] = $value;
                    }
                    break;

                case 'type':
                    if (in_array($value, array('vendor.model', 'book', 'audiobook', 'artist.title', 'tour', 'ticket', 'event-ticket'))) {
                        $offer['type'] = $value;
                    }
                    break;

                case 'available':
                    $offer['available'] = ($value ? 'true' : 'false');
                    break;

                case 'param':
                    if (is_array($value)) {
                        $offer['param'] = $value;
                    }
                    break;

                default:
                    break;
            }
        }

        $type = isset($offer['type']) ? $offer['type'] : '';

        $allowed_tags = array('url'=>0, 'buyurl'=>0, 'price'=>1, 'wprice'=>0, 'currencyId'=>1, 'xCategory'=>0, 'categoryId'=>1, 'picture'=>0, 'store'=>0, 'pickup'=>0, 'delivery'=>0, 'deliveryIncluded'=>0, 'local_delivery_cost'=>0, 'orderingTime'=>0);

        switch ($type) {
            case 'vendor.model':
                $allowed_tags = array_merge($allowed_tags, array('typePrefix'=>0, 'vendor'=>1, 'vendorCode'=>0, 'model'=>1, 'provider'=>0, 'tarifplan'=>0));
                break;

            case 'book':
                $allowed_tags = array_merge($allowed_tags, array('author'=>0, 'name'=>1, 'publisher'=>0, 'series'=>0, 'year'=>0, 'ISBN'=>0, 'volume'=>0, 'part'=>0, 'language'=>0, 'binding'=>0, 'page_extent'=>0, 'table_of_contents'=>0));
                break;

            case 'audiobook':
                $allowed_tags = array_merge($allowed_tags, array('author'=>0, 'name'=>1, 'publisher'=>0, 'series'=>0, 'year'=>0, 'ISBN'=>0, 'volume'=>0, 'part'=>0, 'language'=>0, 'table_of_contents'=>0, 'performed_by'=>0, 'performance_type'=>0, 'storage'=>0, 'format'=>0, 'recording_length'=>0));
                break;

            case 'artist.title':
                $allowed_tags = array_merge($allowed_tags, array('artist'=>0, 'title'=>1, 'year'=>0, 'media'=>0, 'starring'=>0, 'director'=>0, 'originalName'=>0, 'country'=>0));
                break;

            case 'tour':
                $allowed_tags = array_merge($allowed_tags, array('worldRegion'=>0, 'country'=>0, 'region'=>0, 'days'=>1, 'dataTour'=>0, 'name'=>1, 'hotel_stars'=>0, 'room'=>0, 'meal'=>0, 'included'=>1, 'transport'=>1, 'price_min'=>0, 'price_max'=>0, 'options'=>0));
                break;

            case 'event-ticket':
                $allowed_tags = array_merge($allowed_tags, array('name'=>1, 'place'=>1, 'hall'=>0, 'hall_part'=>0, 'date'=>1, 'is_premiere'=>0, 'is_kids'=>0));
                break;

            default:
                $allowed_tags = array_merge($allowed_tags, array('name'=>1, 'vendor'=>0, 'vendorCode'=>0));
                break;
        }

        $allowed_tags = array_merge($allowed_tags, array('aliases'=>0, 'additional'=>0, 'description'=>0, 'sales_notes'=>0, 'promo'=>0, 'manufacturer_warranty'=>0, 'country_of_origin'=>0, 'downloadable'=>0, 'adult'=>0, 'barcode'=>0));

        $required_tags = array_filter($allowed_tags);

        if (sizeof(array_intersect_key($data, $required_tags)) != sizeof($required_tags)) {
            return;
        }

        $data = array_intersect_key($data, $allowed_tags);
//		if (isset($data['tarifplan']) && !isset($data['provider'])) {
//			unset($data['tarifplan']);
//		}

        $allowed_tags = array_intersect_key($allowed_tags, $data);

        // Стандарт XML учитывает порядок следования элементов,
        // поэтому важно соблюдать его в соответствии с порядком описанным в DTD
        $offer['data'] = array();
        foreach ($allowed_tags as $key => $value) {
            $offer['data'][$key] = $this->prepareField($data[$key]);
        }

        //$this->offers[] = $offer;
        return $offer;
    }

    /**
     * Фрмирование элемента
     *
     * @param array $attributes
     * @param string $element_name
     * @param string $element_value
     * @return string
     */
    private function getElement($attributes, $element_name, $element_value = '') {
        $retval = '<' . $element_name . ' ';
        foreach ($attributes as $key => $value) {
            $retval .= $key . '="' . $value . '" ';
        }
        $retval .= $element_value ? '>' . $this->eol . $element_value . '</' . $element_name . '>' : '/>';
        $retval .= $this->eol;

        return $retval;
    }

    /**
     * Преобразование массива в теги
     *
     * @param array $tags
     * @return string
     */
    private function array2Tag($tags) {
        $retval = '';
        foreach ($tags as $key => $value) {
            $retval .= '<' . $key . '>' . $value . '</' . $key . '>' . $this->eol;
        }

        return $retval;
    }

    /**
     * Преобразование массива в теги параметров
     *
     * @param array $params
     * @return string
     */
    private function array2Param($params) {
        $retval = '';
        foreach ($params as $param) {
            $retval .= '<param name="' . $this->prepareField($param['name']);
            if (isset($param['unit'])) {
                $retval .= '" unit="' . $this->prepareField($param['unit']);
            }
            $retval .= '">' . $this->prepareField($param['value']) . '</param>' . $this->eol;
        }

        return $retval;
    }

    /**
     * Подготовка текстового поля в соответствии с требованиями Яндекса
     * Запрещаем любые html-тэги, стандарт XML не допускает использования в текстовых данных
     * непечатаемых символов с ASCII-кодами в диапазоне значений от 0 до 31 (за исключением
     * символов с кодами 9, 10, 13 - табуляция, перевод строки, возврат каретки). Также этот
     * стандарт требует обязательной замены некоторых символов на их символьные примитивы.
     * @param string $text
     * @return string
     */
    private function prepareField($field) {
        $field = htmlspecialchars_decode($field);
        $field = strip_tags($field);
        $from = array('"', '&', '>', '<', '\'');
        $to = array('&quot;', '&amp;', '&gt;', '&lt;', '&apos;');
        $field = str_replace($from, $to, $field);
        //if ($this->from_charset != 'windows-1251') {
//			$field = iconv($this->from_charset, 'windows-1251//IGNORE', $field);
        //}
        $field = preg_replace('#[\x00-\x08\x0B-\x0C\x0E-\x1F]+#is', ' ', $field);

        return trim($field);
    }

    protected function getPath($category_id, $current_path = '') {
        if (isset($this->categories[$category_id])) {
            $this->categories[$category_id]['export'] = 1;

            if (!$current_path) {
                $new_path = $this->categories[$category_id]['id'];
            } else {
                $new_path = $this->categories[$category_id]['id'] . '_' . $current_path;
            }

            if (isset($this->categories[$category_id]['parentId'])) {
                return $this->getPath($this->categories[$category_id]['parentId'], $new_path);
            } else {
                return $new_path;
            }

        }
    }

    function filterCategory($category) {
        return isset($category['export']);
    }

    /**
     * Преобразование слов, содержащих более 4 заглавных букв в строчные
     * @param $string - строка над которой делаем преобразования
     * @return string - возвращаемая строка с преобразованными словами
     */
    function fixCapsWords($string) {
        return preg_replace_callback(
            '/[a-z0-9а-яё]*[A-ZА-ЯЁ]{4,}[a-z0-9а-яё]*/u',
            function ($x) {
                $x = $x[0];
                $e = "utf-8";
                $x2 = array();
                foreach (explode(", ", $x) as $j => $y) {
                    if ($j == 0) $x2[] = mb_strtoupper(mb_substr($y, 0, 1, $e), $e) . mb_strtolower(mb_substr($y, 1, mb_strlen($y, $e), $e), $e);
                    else $x2[] = mb_strtolower($y, $e);
                }
                $x = implode(", ", $x2);
                return $x;
            },
            $string);
    }
}


?>